///http://arduino.esp8266.com/stable/package_esp8266com_index.json
#include <ESP8266WiFi.h>
#include <TimeLib.h>
#include <WiFiUdp.h>
#include <PubSubClient.h>
#include <EEPROM.h>



/////////////Вафля////////////////////////////////////////////////////////////
const char* ssid = ""; //  your network SSID (name)
const char* password = ""; // your network password

//////////Настройки MQTT//////////////////////////////////////////////////////
const char *mqtt_server = "192.168.11.4"; // Имя сервера MQTT
const int mqtt_port = 1883; // Порт для подключения к серверу MQTT
const char *mqtt_user = "heinz"; // Логи для подключения к серверу MQTT
const char *mqtt_pass = "192422901"; // Пароль для подключения к серверу MQTT

WiFiClient wclient;
PubSubClient client(wclient);

//////////NTP//////////////////////////////////////////////////////
static const char ntpServerName[] = "1.europe.pool.ntp.org";
const int timeZone = 8;
const int NTP_PACKET_SIZE = 48; // NTP time is in the first 48 bytes of message
byte packetBuffer[NTP_PACKET_SIZE]; //buffer to hold incoming & outgoing packets
WiFiUDP Udp;
unsigned int localPort = 2390;      // local port to listen for UDP packets

///////Переменные/////////////////////////////////////////////////////////////
unsigned long curTime = 0; //для организации циклов прерывания
unsigned long publicTime = 0; //для организации циклов прерывания
unsigned int cicle_update = 3000; //основной цикл минуты
unsigned int cicle_public = 300000; // цикл для публикации данных
unsigned long lastReconnectAttempt = 0;
bool pumping = false; //активирован режим полива
bool debug = true;
char message[30]; //сюда кэшируем дату

//////Входа///////////////////////////////////////////////////////////////////
byte pump = 5;
//макс изм напряжение ADC это 1в, а не 3.3.
byte level = 4;
byte valve1 = 14;
byte valve2 = 12;
byte valve3 = 13;

//Время работы насоса в секундах
int f1l_pump_time = 10;
int fl2_pump_time = 10;
int fl3_pump_time = 10;

//Для работы автополива по времени
//сюда пишем день когда поливали в последний раз
int lastPumpp[3];
bool Pump[3] = {false, false, false};

//интервал полива в днях
int fl_interval[3] = {3, 3, 3};

///адреса хранения данных о поливе
#define fl1_pump_time_adr 0
#define fl2_pump_time_adr (fl1_pump_time_adr+sizeof(int))
#define fl3_pump_time_adr (fl2_pump_time_adr+sizeof(int))
#define fl1_interval_adr (fl3_pump_time_adr+sizeof(int))
#define fl2_interval_adr (fl1_interval_adr+sizeof(int))
#define fl3_interval_adr 21

void watering(byte valve, byte del);
bool writeEEPROM(int val, byte adr);

void callback(char* topic, byte* payload, unsigned int length) {
  String str = "";
  if (debug)Serial.print("Message arrived [");
  if (debug)Serial.print(topic);
  if (debug)Serial.print("] ");
  if (debug) Serial.print(length);
  if (debug) Serial.print("payload=");

  for (int i = 0; i < length; i++) {
    if (debug) Serial.print((char)payload[i]);
    str += (char)payload[i];
  }
  if (debug) Serial.println();

  if ((String)topic == "flower_k/manual") {
    if (str == "all") {
      if (digitalRead(level) || debug) {
        pumping_all(f1l_pump_time, fl2_pump_time, fl3_pump_time);
        lastPumpp[0] = day();
        lastPumpp[1] = day();
        lastPumpp[2] = day();
      } else  level_publish();
    }
    if (str == "fl1_pump_time") {
      if (digitalRead(level) || debug) {
        watering(valve1, f1l_pump_time);
        lastPumpp[0] = day();
      } else level_publish();
    }
    if (str == "fl2_pump_time") {
      if (digitalRead(level) || debug) {
        watering(valve2, fl2_pump_time);
        lastPumpp[1] = day();
      } else level_publish();
    }
    if (str == "fl3_pump_time") {
      if (digitalRead(level) || debug) {
        watering(valve3, fl3_pump_time);
        lastPumpp[2] = day();
      } else level_publish();
    }
  }

  if ((String)topic == "flower_k/time_fl1") {
    if (debug)Serial.println(str.toInt());
    if (writeEEPROM(str.toInt(), fl1_pump_time_adr)) f1l_pump_time = str.toInt();
  }

  if ((String)topic == "flower_k/time_fl2") {
    if (debug)Serial.println(str.toInt());
    if ( writeEEPROM(str.toInt(), fl2_pump_time_adr)) fl2_pump_time = str.toInt();
  }

  if ((String)topic == "flower_k/time_fl3") {
    if (debug)Serial.println(str.toInt());
    if ( writeEEPROM(str.toInt(), fl3_pump_time_adr)) fl3_pump_time = str.toInt();
  }

  if ((String)topic == "flower_k/interval_fl1") {
    if (debug)Serial.println(str.toInt());
    if ( writeEEPROM(str.toInt(), fl1_interval_adr)) fl_interval[0] = str.toInt();
  }
  if ((String)topic == "flower_k/interval_fl2") {
    if (debug)Serial.println(str.toInt());
    if ( writeEEPROM(str.toInt(), fl2_interval_adr)) fl_interval[1] = str.toInt();
  }
  if ((String)topic == "flower_k/interval_fl3") {
    if (debug)Serial.println(str.toInt());
    if ( writeEEPROM(str.toInt(), fl3_interval_adr)) fl_interval[2] = str.toInt();
  }
}

// send an NTP request to the time server at the given address
void sendNTPpacket(IPAddress & address)
{
  // set all bytes in the buffer to 0
  memset(packetBuffer, 0, NTP_PACKET_SIZE);
  // Initialize values needed to form NTP request
  // (see URL above for details on the packets)
  packetBuffer[0] = 0b11100011;   // LI, Version, Mode
  packetBuffer[1] = 0;     // Stratum, or type of clock
  packetBuffer[2] = 6;     // Polling Interval
  packetBuffer[3] = 0xEC;  // Peer Clock Precision
  // 8 bytes of zero for Root Delay & Root Dispersion
  packetBuffer[12] = 49;
  packetBuffer[13] = 0x4E;
  packetBuffer[14] = 49;
  packetBuffer[15] = 52;
  // all NTP fields have been given values, now
  // you can send a packet requesting a timestamp:
  Udp.beginPacket(address, 123); //NTP requests are to port 123
  Udp.write(packetBuffer, NTP_PACKET_SIZE);
  Udp.endPacket();
}

time_t getNtpTime()
{
  IPAddress ntpServerIP; // NTP server's ip address
  while (Udp.parsePacket() > 0) ; // discard any previously received packets
  Serial.println("Transmit NTP Request");
  // get a random server from the pool
  Udp.begin(localPort);
  WiFi.hostByName(ntpServerName, ntpServerIP);
  Serial.print(ntpServerName);
  Serial.print(": ");
  Serial.println(ntpServerIP);
  sendNTPpacket(ntpServerIP);
  uint32_t beginWait = millis();
  while (millis() - beginWait < 1500) {
    int size = Udp.parsePacket();
    if (size >= NTP_PACKET_SIZE) {
      Serial.println("Receive NTP Response");
      Udp.read(packetBuffer, NTP_PACKET_SIZE);  // read packet into the buffer
      unsigned long secsSince1900;
      // convert four bytes starting at location 40 to a long integer
      secsSince1900 =  (unsigned long)packetBuffer[40] << 24;
      secsSince1900 |= (unsigned long)packetBuffer[41] << 16;
      secsSince1900 |= (unsigned long)packetBuffer[42] << 8;
      secsSince1900 |= (unsigned long)packetBuffer[43];
      return secsSince1900 - 2208988800UL + timeZone * SECS_PER_HOUR;
      Udp.flush();
      Udp.stop();
    }
  }
  Serial.println("No NTP Response :-(");
  return 0; // return 0 if unable to get the time
}

void setup() {
  Serial.begin(115200);

  pinMode(pump, OUTPUT);
  pinMode(valve1, OUTPUT);
  pinMode(valve2, OUTPUT);
  pinMode(valve3, OUTPUT);
  pinMode(level, INPUT);

  client.setServer(mqtt_server, mqtt_port);
  client.setCallback(callback);

  wifi_update();
  mqqt_check();
  curTime = millis();
  publicTime = millis();

  setSyncProvider(getNtpTime);
  setSyncInterval(259200);//интервал синхронизации - 3суток
  checkEEPROM();//смотрим че там в ПЗУ, если что обновляем
  lastdata_publish(now());
  intervalSet(now());
  if (debug) Serial.println("Setup_end");
}

//метод для управления насосами и клапанами
void watering(byte valve, byte del) {
  if (debug) Serial.println("Pumping");
  digitalWrite(pump, HIGH);
  //delay(100);
  digitalWrite(valve, HIGH);
  delay(del * 1000);
  digitalWrite(pump, LOW);
  digitalWrite(valve, LOW);
  delay(100);
}


//метод для полива всех банок сразу, не выключаем насос
void pumping_all(byte del, byte del2, byte del3) {
  if (debug)Serial.println("Pumping_ALL");
  digitalWrite(pump, HIGH);
  delay(100);
  digitalWrite(valve1, HIGH);
  delay(del * 1000);
  digitalWrite(valve1, LOW);
  digitalWrite(valve2, HIGH);
  delay(del2 * 1000);
  digitalWrite(valve2, LOW);
  digitalWrite(valve3, HIGH);
  delay(del3 * 1000);
  digitalWrite(valve3, LOW);
  digitalWrite(pump, LOW);
  delay(100);
}

bool wifi_status() {
  if (WiFi.status() == WL_CONNECTED) return true;
  else return false;
}

void wifi_update() {
  if (debug) Serial.println("Wifi_setup");
  if (!wifi_status()) {
    byte i = 0;
    Serial.print("Connecting to ");
    Serial.println(ssid);
    WiFi.mode(WIFI_STA);
    WiFi.begin(ssid, password);
    //пробуем подконектится 10 раз с интервалом в 3 сек.
    while (i < 4 && !wifi_status()) {
      Serial.print("_");
      Serial.print(i);
      delay(3000);
      i++;
    }
    Serial.println();
    if (wifi_status()) {
      Serial.println("WiFi connected");
      Serial.println("IP address: ");
      Serial.println(WiFi.localIP());
    } else Serial.println("Sync stopped, wifi is not found");
  }
}

boolean reconnect() {
  if (debug) Serial.println("Reconnect");
  if (client.connect("ESP8266Pump", mqtt_user, mqtt_pass)) {
    if (debug) Serial.println("MQQT Connection UP");
   // client.publish("flower_k/lastday", "");
   // client.publish("flower_k/level", "");
   // client.publish("flower_k/curTime", "");

    client.subscribe("flower_k/manual");
    client.subscribe("flower_k/time_fl1");
    client.subscribe("flower_k/time_fl2");
    client.subscribe("flower_k/time_fl3");
    client.subscribe("flower_k/interval_fl1");
    client.subscribe("flower_k/interval_fl2");
    client.subscribe("flower_k/interval_fl3");
  }
  return client.connected();
}

void mqqt_check() {
  //if (debug) Serial.print("MQQT");
  if (!client.connected()) {
    if (millis() - lastReconnectAttempt > 5000) {
      if (debug) Serial.println(millis());
      lastReconnectAttempt = millis();
      if (reconnect()) {
        lastReconnectAttempt = 0;
      }
    }
    //if (debug) Serial.println(" reconnect");
  } else {
    client.loop();
    //if (debug) Serial.println(" loop");
  }
}

void level_publish() {
  if (debug) Serial.println("Publish_level");
  char message[10];
  String str;
  if (debug) Serial.println(digitalRead(level));
  if (digitalRead(level)) {
    str = "low";
  } else {
    str = "high";
  }
  str.toCharArray(message, 10);
  if (debug) Serial.println(message);
  client.publish("flower_k/level", message);
}

void lastdata_publish(time_t time_pump) {
  if (debug) Serial.println("Publish_date");
  String str;
  str += day(time_pump);
  str += '.';
  str += month(time_pump);
  str += '.';
  str += year(time_pump);
  str += '/';
  str += hour(time_pump);
  str += ':';
  str += minute(time_pump);
  str += '/';
  str += weekday(time_pump);
  str.toCharArray(message, 20);
  if (debug) Serial.println(message);
  client.publish("flower_k/curTime", message);
}

void interval_last_publish() {
  if (debug) Serial.println("Publish_interval");
  String str;
  str += lastPumpp[0];
  str += ' ';
  str += lastPumpp[1];
  str += ' ';
  str += lastPumpp[2];
  str.toCharArray(message, 10);
  if (debug) Serial.println(message);
  client.publish("flower_k/lastday", message);
}

void interval_publish() {
  if (debug) Serial.println("Publish_interval_pump");
  String str;
  str += fl_interval[0];
  str += ' ';
  str += fl_interval[1];
  str += ' ';
  str += fl_interval[2];
  str.toCharArray(message, 10);
  if (debug) Serial.println(message);
  client.publish("flower_k/interval", message);
}

void pumptime_publish() {
  if (debug) Serial.println("Publish_pump_interval");
  String str;
  str += f1l_pump_time;
  str += ' ';
  str += fl2_pump_time;
  str += ' ';
  str += fl3_pump_time;
  str.toCharArray(message, 10);
  if (debug) Serial.println(message);
  client.publish("flower_k/pumptime", message);
}

void intervalSet(time_t t) {
  t = now();
  for (int i = 0; i < 3; i++) {
    lastPumpp[i] = day(t);
    if (debug) Serial.print(lastPumpp[i]);
    if (debug) Serial.print(" ");
  }
  if (debug) Serial.println();
  interval_publish();
}

void datachek() {
  time_t t = now();
  for (int i = 0; i < 3; i++) {
    if (!(day(t) <  lastPumpp[i])) {
      if (day(t) - lastPumpp[i] >= fl_interval[i]) {
        Pump[i] = true;
      }
    } else {
      if ((day(t) + 30) - lastPumpp[i] >= fl_interval[i]) {
        Pump[i] = true;
      }
    }
  }
}

void pumpReady() {
  if (digitalRead(level) || debug) {
    for (int i = 0; i < 3; i++) {
      if (Pump[i])
        if (hour() == 12)//час как надо
          if (minute() >= 2) {
            if (debug) Serial.println("pump time!");
            if (i == 0)
              watering(valve1, f1l_pump_time);
            if (i == 1)
              watering(valve2, fl2_pump_time);
            if (i == 2)
              watering(valve3, fl3_pump_time);
            Pump[i] = false;
            lastPumpp[i] = day();
          }
    }
  } else  level_publish();
}

void loop() {
  //3600000 = час
  //86400000 = сутки
  mqqt_check();
  //основной цикл 3сек
  if (millis() > (curTime + cicle_update)) {
    curTime = millis();

    datachek();
    pumpReady();
    //chkpump();
  } else if (curTime > millis()) {
    curTime = millis();
    Serial.println("Overflov cicle_update");
  }

  // цикл для обновления данных
  if (millis() > (publicTime + cicle_public)) {
    publicTime = millis();

    wifi_update();

    lastdata_publish(now());
    level_publish();
    interval_publish();
    interval_last_publish();
    pumptime_publish();
    
  } else if (publicTime > millis()) {
    publicTime = millis();
    Serial.println("Overflov cicle_hour");
  }
}

//метод для проверки параметров записанных в пзу
//если то что лежит в еепром отлшичается от дефолта то обновляем.
void checkEEPROM() {
  int fl1 = 255;
  int fl2 = 255;
  int fl3 = 255;
  int if1 = 255;
  int if2 = 255;
  int if3 = 255;
  /*адрес   что лежит
    //Время работы насоса в секундах
    int f1l_pump_time = 10;
    int fl2_pump_time = 10;
    int fl3_pump_time = 10;

    //интервал полива в часах
    int fl_interval[0] = 56;
    int fl_interval[1] = 56;
    int fl_interval[2] = 56;

    #define fl1_pump_time_adr 0
    #define fl2_pump_time_adr (fl1_pump_time_adr+sizeof(f1l_pump_time))
    #define fl3_pump_time_adr (fl2_pump_time_adr+sizeof(fl2_pump_time))
    #define timeadr (fl3_pump_time_adr+sizeof(fl3_pump_time))
    #define fl_interval[0]_adr (timeadr+sizeof(message))
    #define fl_interval[1]_adr (fl_interval[0]_adr+sizeof(fl_interval[0]))
    #define fl_interval[2]_adr (fl_interval[1]_adr+sizeof(fl_interval[1]))
  */
  EEPROM.begin(50);
  EEPROM.get(fl1_pump_time_adr, fl1);
  EEPROM.get(fl2_pump_time_adr, fl2);
  EEPROM.get(fl3_pump_time_adr, fl3);
  EEPROM.get(fl1_interval_adr, if1);
  EEPROM.get(fl2_interval_adr, if2);
  EEPROM.get(fl3_interval_adr, if3);

  if (debug) Serial.print("fl3_interval_adr= ");
  if (debug) Serial.print(fl3_interval_adr);
  if (debug) Serial.println(" ");

  if (debug) Serial.print("fl2_interval_adr= ");
  if (debug) Serial.print(fl2_interval_adr);
  if (debug) Serial.println(" ");

  if (debug)Serial.println(fl1);
  if (debug)Serial.println(fl2);
  if (debug)Serial.println(fl3);
  if (debug)Serial.println(if1);
  if (debug)Serial.println(if2);
  if (debug)Serial.println(if3);
  //  if (debug)Serial.println(message);

  //кусок тупого кода, не хотелось с массивом заморачиваться
  if (fl1 != f1l_pump_time & fl1 != 0 & fl1 != 255 & fl1 > 0)f1l_pump_time = fl1;
  if (fl2 != fl2_pump_time & fl2 != 0 & fl2 != 255 & fl2 > 0)fl2_pump_time = fl2;
  if (fl3 != fl3_pump_time & fl3 != 0 & fl3 != 255 & fl3 > 0)fl3_pump_time = fl3;
  if (if1 != fl_interval[0] & if1 != 0 & if1 != 255 & if1 > 0)fl_interval[0] = if1;
  if (if2 != fl_interval[1] & if2 != 0 & if2 != 255 & if2 > 0)fl_interval[1] = if2;
  if (if3 != fl_interval[2] & if3 != 0 & if3 != 255 & if3 > 0)fl_interval[2] = if3;
  EEPROM.end();
}

bool writeEEPROM(int val, byte adr) {
  int param = 0;
  if (debug) Serial.print("adr= ");
  if (debug) Serial.print(adr);
  if (debug) Serial.println(" ");
  EEPROM.begin(25);
  EEPROM.get(adr, param);
  if (debug) Serial.print("param= ");
  if (debug) Serial.print(param);
  if (debug) Serial.println(" ");
  if (param != val) {
    EEPROM.put(adr, val);
    if (debug) Serial.print("write ");
    if (debug) {
      EEPROM.get(adr, param);
      Serial.println(param);
    }
    EEPROM.commit();
    return true;
  } else return false;
  EEPROM.end();
}

bool writeEEPROM_str(String val, byte adr) {
  String param ;
  EEPROM.begin(25);
  EEPROM.get(adr, param);
  if (debug) Serial.println(param);
  if (param != val) {
    EEPROM.put(adr, val);
    if (debug) Serial.print("write ");
    if (debug) {
      EEPROM.get(adr, param);
      Serial.println(param);
    }
    EEPROM.commit();
    return true;
  } else return false;
  EEPROM.end();
}
