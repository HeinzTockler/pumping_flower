#include <RTC.h>
#include <Wire.h>
#include <LiquidCrystal_I2C.h>

RTC time;
LiquidCrystal_I2C lcd(0x3F, 2, 1, 0, 4, 5, 6, 7, 3, POSITIVE);
//Константы
#define del 1000
#define hour_of_measure 15
#define minute_of_measure 30

//аналоги
byte GrHum1 = 0;
byte GrHum2 = 1;
byte GrHum3 = 2;
//дискреты
byte HumCon1 = 2;
byte HumCon2 = 3;
byte HumCon3 = 4;
byte pump1 = 5;
//byte pump2 = 6;
byte HValve1 = 7;
byte HValve2 = 8;
byte HValve3 = 9;
//Переменные
unsigned long currentTime = 0;
unsigned long loopTime = 0;

byte Splink_time[3] {5, 5, 5};
int hum[6];
bool needSplink[3] {true, true, true};

byte i = 0, j = 0;
//int hum1, hum2, hum3;
//bool temp = false;
bool inTime = false;



//инициализация лсд панельки
void lcd_Preset() {
  lcd.begin(16, 2);
  // lcd.init();
  lcd.backlight();
  // lcd.home ();
}

void setup() {
  //Сервисная шляпа
  lcd_Preset();
  Wire.begin();
  time.begin(RTC_DS3231);//Инициализация модуля времени

  //инициализируем переменные для прерываний
  currentTime = millis();
  loopTime = currentTime;
  //конфигурируем пины
  pinMode(HumCon1, OUTPUT);
  pinMode(HumCon2, OUTPUT);
  pinMode(HumCon3, OUTPUT);
  pinMode(pump1, OUTPUT);
  //pinMode(pump2, OUTPUT);
  pinMode(HValve1, OUTPUT);
  pinMode(HValve2, OUTPUT);
  pinMode(HValve3, OUTPUT);
  //  pinMode(Ntc0, INPUT);
  Serial.begin(9600);
  inTime = true;
}

void loop() {
  //если час измерений еще не подошел то ходим мимо
  /*if ((int)time.Hours = hour_of_measure && (int)time.minutes = minute_of_measure && !inTime) {
    inTime = true;
    currentTime = millis();
    loopTime = currentTime;
    }*/

  if (inTime) {
    //обновляем текущее время
    currentTime = millis();
    if (currentTime >= (loopTime + del)) {// сравниваем текущий таймер с переменной loopTime + del
      //делаем тики каждые 1 секунд и в зависимости от алгоритма шагаем
      i++;
      //time.gettime();
      // Serial.println((String)"time= "+time.Hours);
      //Serial.println((String)"Hours= "+time.minutes);
      lcd.setCursor(14, 1);
      lcd.print((String)i);
      MainAlgoritm();
      loopTime = currentTime;// в loopTime записываем новое значение
    } else if (loopTime > currentTime) {
      //переполнилась переменная, поэтому обнуляем параметры
      loopTime = currentTime;
      Serial.println("overflow");
    }
  }

}

void MainAlgoritm() {
  //Serial.println((String)"Main " + i);
  switch (i) {
    case 1:
      //для начала врубаем сенсоры
      SensorOn(true);
      vipeArch();
      break;
    case 5:
      //читаем что у нас там вышло с влажнстью
      getHumGround();
      SensorOn(false);
      break;
    case 7:
      Serial.println((String)"========================OSNOVA============================");
      Splink();
      break;
    case 20:
      SensorOn(true);
      break;
    case 21:
      humOfset();
      getHumGround();
      SensorOn(false);
      break;
    case 22:
      ControlSplink();
      break;
    case 30:
      i = 0;
      inTime = false;
      break;
  }
}

//метод для управлфения сенсорами, включаем раз в день, чтобы постоянно не шел электрлиз
void SensorOn(bool command) {
  digitalWrite(HumCon1, command);
  digitalWrite(HumCon2, command);
  digitalWrite(HumCon3, command);
}

//метод считывает данные с сенсоров и пишет в архив
void getHumGround() {
  hum[0] = analogRead(GrHum1);
  hum[1] = analogRead(GrHum2);
  hum[2] = analogRead(GrHum3);

  Serial.println((String)"h1=" + hum[0]);
  Serial.println((String)"h2=" + hum[1]);
  Serial.println((String)"h3=" + hum[2]);

  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("h1=");
  lcd.setCursor(3, 0);
  lcd.print((String)hum[0]);
  lcd.setCursor(7, 0);
  lcd.print("h2=");
  lcd.setCursor(10, 0);
  lcd.print((String)hum[1]);
  lcd.setCursor(0, 1);
  lcd.print("h3=");
  lcd.setCursor(3, 1);
  lcd.print((String)hum[2]);
}

//метод для обнуления архива с влажностью почвы
void vipeArch() {
  for (byte k = 0; k < 3; k++) {
    hum[k] = 0;
    hum[k + 3] = 0;
    needSplink[k] = true;
  }
}

//смещает данные в архиве
void humOfset() {
  Serial.println((String)"SMESHAEM archiv!!!");
  int tmp = 0;
  for (byte k = 0; k < 3; k++) {
    hum[k + 3] = hum[k];
  }
}

//метод для анализа
//считаем что если показатель влажности меньше 600 то увлажнено норм
//если 1023 то земля полностью сухая
//если меньше 500 то пиздец какая мокрая
void Splink() {
  byte valve = 0;
  for (byte k = 0; k < 3; k++) {
    valve = switchingValve(k);
    if (hum[k] >= 1000)
    {
      //совсем сухо надо лить
      PumpControl(valve, Splink_time[k] * 1000);
      Serial.println((String)"Polivaem po polnoi!!! " + valve + " dlit= " + Splink_time[k] * 1000);
    } else if (hum[k] >= 800) {
      //можно полить чутка
      PumpControl(valve, Splink_time[k] * 1000 / 2 );
      Serial.println((String)"Polivaem polovinku!!! " + valve + " dlit= " + Splink_time[k] * 1000 / 2);
    } else if (hum[k] >= 600) {
      //можно полить чутка
      PumpControl(valve, Splink_time[k] * 1000 / 3 );
      Serial.println((String)"Polivaem polovinku!!! " + valve + " dlit= " + Splink_time[k] * 1000 / 3);
    } else {
      //очень мокро не льем
    }
  }
}

byte switchingValve(byte k) {
  byte valve = 0;
  switch (k) {
    case 0:
      valve = HValve1;
      break;
    case 1:
      valve = HValve2;
      break;
    case 2:
      valve = HValve3;
      break;
  }
  return valve;
}

/*
  float getGroundHumidity(byte pin, byte conection) {
  float res = 0;
  digitalWrite(conection, HIGH);
  delay(1000);
  res = analogRead(GrHum1);
  digitalWrite(conection, LOW);
  return res;
  }*/

//метод управления насосами и клапанами (pin) и времененем работы клапаном delayT
void PumpControl(byte pin, int delayT) {
  digitalWrite(pin, HIGH);
  digitalWrite(pump1, HIGH);
  delay(delayT);
  digitalWrite(pin, LOW);
  digitalWrite(pump1, LOW);
}

//проверяем как мы все полили
void ControlSplink() {
  j++;//считаем сколько раз мы сюда зашли
  Serial.println((String)"========================Prohod " + j + "============================");
  int tmp = 0;
  byte valve = 0;

  for (byte k = 0; k < 3; k++) {
    tmp = hum[k + 3] - hum[k];

    if (needSplink[k]) {
      if (tmp < 0) {
        //полили а стало только хуже значит надо долить еще
        Splink_time[k] += 4;
        needSplink[k] = true;
      } else if (tmp < 100) {
        Splink_time[k] += 3;
        needSplink[k] = true;
      } else if (tmp < 200) {
        Splink_time[k] += 2;
        needSplink[k] = true;
      } else if (tmp < 300) {
        Splink_time[k] += 1;
        needSplink[k] = false;
      } else {
        //полили пиздато совсем
        needSplink[k] = false;
        if (Splink_time[k] >= 5)
          Splink_time[k] -= 2;
      }
      Serial.println((String)"tmp= " + tmp);

      if (needSplink[k]) {
        valve = switchingValve(k);
        Serial.println((String)"DOlivaem polovinku!!! " + valve + " dlit= " + Splink_time[k] * 1000 / 3);
        PumpControl(valve, Splink_time[k] * 1000 / 3 );
      }

    }
  }
  //делаем 5 проходов
  if (j < 5) {
    i = 15;
  } else {
    i = 29;
    j = 0;
  }
}

